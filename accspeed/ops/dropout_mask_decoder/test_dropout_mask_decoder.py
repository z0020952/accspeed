# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import functools
import os
import shutil

import mindspore as ms
import mindspore.context as context
import mindspore.nn as nn
import numpy as np
import pytest
from mindspore import Profiler
from mindspore import Tensor
from mindspore import dtype as mstype
from mindspore import ops

from dropout_mask_decoder_primitive import DropoutMaskDecoderPrimitive

context.set_context(mode=context.GRAPH_MODE, device_target="Ascend", device_id=3)
os.environ["GLOG_v"] = "3"
os.environ["ASCEND_GLOBAL_LOG_LEVEL"] = "3"
os.environ["ASCEND_SLOG_PRINT_TO_STDOUT"] = "0"

shutil.rmtree("./rank_0", ignore_errors=True)
shutil.rmtree("./kernel_meta", ignore_errors=True)


class DropoutMaskDecoder(nn.Cell):
    def __init__(self, bsz):
        super(DropoutMaskDecoder, self).__init__()
        self.mask_decoder = DropoutMaskDecoderPrimitive(bsz)
        self.tensor_sz_op = ops.Size()

    def construct(self, input_mask):
        i_mask_bytes = self.tensor_sz_op(input_mask)
        i_mask_bytes_aligned = (i_mask_bytes + 127) // 128 * 128
        pad_len = i_mask_bytes_aligned - i_mask_bytes
        if pad_len > 0:
            input_mask = ops.pad(input_mask, [0, pad_len], mode='constant', value=0)

        return self.mask_decoder(input_mask)


class MsDropoutMaskNet(nn.Cell):
    def __init__(self, bsz):
        """
        :param bsz:   # bsz, head_num, seq_len, seq_len
        """
        super(MsDropoutMaskNet, self).__init__()
        self.fill_v2 = ops.FillV2()
        self.do_domask = ops.DropoutDoMask()
        self.tensor_one = Tensor(1.0, mstype.float16)
        self.tensor_sz_op = ops.Size()
        self.bsz = bsz
        self.keep_prob = Tensor(1.0, dtype=mstype.float16)

    def construct(self, input_mask):
        """
            :param input_mask: input drop mask of dtype UINT8
        :return:o  Tensor with shape same as input byte_mask, with dtype of u8
        """
        i_mask_bytes = self.tensor_sz_op(input_mask)
        i_mask_bytes_aligned = (i_mask_bytes + 127) // 128 * 128
        pad_len = i_mask_bytes_aligned - i_mask_bytes
        if pad_len > 0:
            input_mask = ops.pad(input_mask, [0, pad_len], mode='constant', value=0)

        o_mask_bytes = 8 * i_mask_bytes_aligned

        o_mask_sec_dim = o_mask_bytes // self.bsz
        o_mask_shape = (self.bsz, o_mask_sec_dim)
        ones = self.fill_v2(o_mask_shape, self.tensor_one)
        return self.do_domask(ones, input_mask, self.keep_prob)


# fmt: off
@pytest.mark.parametrize("input_mask_shape",
    [
        (1, 1),
        (1, 128),
        (2, 128),
        (1, 1024),
        (1, 1025),
        (1, 31 * 1023),
        (1, 32 * 1024 * 1024),
        (2, 16 * 2048 * 2048),
        (1, 8 * 4096 * 4096),
        # (1, 32, 8192, 8192), # Two large for ms to get tensor res.
    ],
    ids=[
        'decode_1B',
        'decode_128B',
        'decode_256B',
        'decode_1K',
        'decode_1K_plus_1B',
        'decode_with_tail',
        'decode_32k',
        'decode_128k',
        'decode_128MB',
         # '8k'
        ]
)
# fmt: on
def test_decoder_precision(input_mask_shape: tuple):
    # i_mask_sz = up_align_128B_input_mask_sz(input_mask_shape)
    # print(f'i_mask_sz = {i_mask_sz}')
    i_mask_sz = functools.reduce(lambda x, y: x * y, input_mask_shape)

    i_mask_np = np.random.randint(0, 256, size=(i_mask_sz,))
    dsl_i_mask = Tensor(i_mask_np, dtype=ms.uint8)
    bsz = input_mask_shape[0]
    dsl_decoder = DropoutMaskDecoder(bsz)
    dsl_res = dsl_decoder(dsl_i_mask).asnumpy()
    print(f'dsl_res.input_mask_shape={dsl_res.shape}')
    print(f'i_mask_np={i_mask_np}')
    print(f'dsl_res={dsl_res}')

    ms_decoder = MsDropoutMaskNet(bsz)
    ms_res = ms_decoder(dsl_i_mask).asnumpy().astype('uint8')
    print(f'ms_res={ms_res}')
    print(f'ms_res={ms_res.dtype}')
    print(f'ms_res={ms_res.shape}')

    assert np.allclose(dsl_res, ms_res)


def up_align_128B_input_mask_sz(input_mask_shape):
    return (functools.reduce(lambda x, y: x * y, input_mask_shape) + 127) // 128 * 128

# fmt: off
@pytest.mark.parametrize('input_mask_shape',
                         [
                             (1, 1),
                             (1, 128),
                             (1, 256),
                             (1, 1024),
                             (1, 1024, 1024),
                             (1, 32//8, 8192, 8192),
                             (1, 40//8, 8192, 8192),
                         ],
                         ids=[
                             '1B',
                             '128B',
                             '256B',
                             '1K',
                             '1M',
                             'xunfei_2.6B_8k_256MB',
                             'xunfei_13B_8k_320MB',
                         ])
# fmt: on
def test_decoder_perf(input_mask_shape:tuple, request):
    i_mask_sz = functools.reduce(lambda x, y: x * y, input_mask_shape)

    i_mask_np = np.random.randint(0, 2, size=(i_mask_sz,))
    i_mask = Tensor(i_mask_np, dtype=ms.uint8)

    test_id = request.node.callspec.id
    prof_path = f'./dropout_mask_decoder_profiling_cmp/{test_id}'
    if os.path.exists(prof_path):
        shutil.rmtree(prof_path, ignore_errors=True)
        
    profiler = Profiler(output_path=prof_path,
                        profile_communication=True)

    bsz = input_mask_shape[0]
    dsl_decoder = DropoutMaskDecoder(bsz=bsz)
    dsl_decoder(i_mask)

    ms_decoder = MsDropoutMaskNet(bsz=bsz)
    ms_decoder(i_mask)

    profiler.analyse()


if __name__ == '__main__':
    test_decoder_precision(input_mask_shape=(1, 1))